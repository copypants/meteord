set -e

bail() {
    echo $1
    exit 1
}

bash $METEORD_DIR/lib/install_node.sh       || bail 'error installing node'
bash $METEORD_DIR/lib/cleanup.sh            || bail 'error during cleanup'
bash $METEORD_DIR/lib/install_meteor.sh     || bail 'error installing meteor'
