set -e

bail() {
    echo $1
    exit 1
}

curl -sL https://install.meteor.com | sed s/--progress-bar/-sL/g | /bin/sh

meteor --version || bail 'meteor --version command failed.'
