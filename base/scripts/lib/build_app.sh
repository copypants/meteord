set -e

export USER=root
export METEOR_ALLOW_SUPERUSER=1
export NPM_CONFIG_PREFIX=/root/.npm-global

COPIED_APP_PATH=/copied-app
BUNDLE_DIR=/tmp/bundle-dir

# sometimes, directly copied folder cause some weird issues
# this fixes that
cp -R /app $COPIED_APP_PATH
cd $COPIED_APP_PATH

echo '%% Installing app npm dependencies.. %%'
npm install --unsafe-perm
echo '%% Done installing app npm dependencies. %%'

echo '%% Installing Meteor Git Packages.. %%'
mgp
echo '%% Done installing Meteor Git Packages. %%'

echo '%% Building meteor bundle.. %%'
meteor build --directory $BUNDLE_DIR --server=http://localhost:3000 --unsafe-perm --allow-superuser
echo '%% Done building meteor bundle. %%'

echo '%% Installing meteor npm dependencies.. %%'
cd $BUNDLE_DIR/bundle/programs/server/
npm install --unsafe-perm
echo '%% Done installing meteor npm dependencies. %%'

mv $BUNDLE_DIR/bundle /built_app

# cleanup
rm -rf $COPIED_APP_PATH
rm -rf $BUNDLE_DIR
