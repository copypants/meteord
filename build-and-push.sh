#!/bin/bash

function bail() {
    echo $1
    exit 1
}

#docker pull copypants.azurecr.io/meteord:base || echo "no existing image."

docker build --cache-from copypants.azurecr.io/meteord:base -t copypants.azurecr.io/meteord:base ./base     || bail "error building base image."
docker build -t copypants.azurecr.io/meteord:onbuild ./onbuild                                              || bail "error building onbuild image."
docker build -t copypants.azurecr.io/meteord:devbuild ./devbuild                                            || bail "error building devbuild image."
docker build -t copypants.azurecr.io/meteord:testbuild ./testbuild                                          || bail "error building testbuild image."

docker push copypants.azurecr.io/meteord:base           || bail "error pushing base image."
docker push copypants.azurecr.io/meteord:onbuild        || bail "error pushing onbuild image."
docker push copypants.azurecr.io/meteord:devbuild       || bail "error pushing devbuild image."
docker push copypants.azurecr.io/meteord:testbuild      || bail "error pushing testbuild image."
