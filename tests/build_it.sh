#!/bin/bash
docker build -t kadirahq/meteord:base ../base
docker build -t kadirahq/meteord:onbuild ../onbuild
docker build -t kadirahq/meteord:devbuild ../devbuild
docker build -t kadirahq/meteord:binbuild ../binbuild